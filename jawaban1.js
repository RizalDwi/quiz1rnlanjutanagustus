import React, {useState, useEffect} from 'react'
import { View, Text } from 'react-native'

export default function jawaban1() {
    const [name, setName] = useState('Jhon Doe')
    useEffect(() => {
        setTimeout(() => {
        setName('Asep')
        }, 3000)
    })

    return (
        <View>
        <Text>{name}</Text>
        </View>
    )
}
