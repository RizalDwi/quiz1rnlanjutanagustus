import React, {useContext} from 'react'
import { View, Text, FlatList } from 'react-native'
import ContextAPI from './ContextAPI.js'

export default function App() {
  const state = useContext(ContextAPI)

  const renderItem = ({item}) => {
    return (
      <View style={{marginVertical: 5 ,borderColor: 'gray', borderWidth: 1, width: '100%', height: 100}}>
          <Text>{item.name}</Text>
          <Text>{item.position}</Text>
      </View>
    )
  }

  return (
    <View>
      <FlatList data={state.name} renderItem={renderItem} />
    </View>
  )
}